#include "cam_lidar_calibration/calibration_data.h"
#include "openga.h"
#include "point_xyzir.h"

#include <cv_bridge/cv_bridge.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <fstream>
#include <image_transport/image_transport.h>
#include <iostream>
#include <math.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Int8.h>
#include <string.h>
#include <string>
#include <string>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf2/convert.h>
#include <utility>
#include <vector>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#define PI 3.141592653589793238463

class optimizer_class
{
public:
  ros::Publisher vis_pub_debug;
  optimizer_class();

  std::vector<double> rotm2eul(cv::Mat mat);
  void sampleCallback(const cam_lidar_calibration::calibration_data::ConstPtr& data);
  void flagCallback(const std_msgs::Int8::ConstPtr& msg);
  void optimize();

private:
  ros::Subscriber calibdata_sub;
  int sample = 0;
  cv::Mat dist_coeff =
      (cv::Mat_<double>(1, 4) << -0.05400957120448697, -0.07842753582468161, 0.09596410068935728, -0.05152529532743679);
  cv::Mat camera_mat = (cv::Mat_<double>(3, 3) << 1176.931662006163, 0.0, 962.2754188883206, 0.0, 1177.715660133758,
                        612.7245350750861, 0.0, 0.0, 1.0);

  struct CameraLidarCalibrationData
  {
    std::vector<std::vector<double>> lidar_normals;
    std::vector<std::vector<double>> lidar_points;
    std::vector<std::vector<double>> camera_normals;
    std::vector<std::vector<double>> camera_points;
    std::vector<std::vector<double>> lidar_corners;
    std::vector<double> pixel_data;

    cv::Mat camera_normals_mat;  // n x 3
    cv::Mat camera_points_mat;   // n x 3
    cv::Mat lidar_points_mat;    // n x 3
    cv::Mat lidar_normals_mat;   // n x 3
    cv::Mat lidar_corners_mat;   // n x 3
    cv::Mat pixel_data_mat;      // 1 x n
  } calibration_data;
};
