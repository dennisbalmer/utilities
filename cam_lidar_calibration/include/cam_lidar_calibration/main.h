#include "point_xyzir.h"
#include "cam_lidar_calibration/calibration_data.h"

#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <cam_lidar_calibration/boundsConfig.h>
#include <cv_bridge/cv_bridge.h>
#include <dynamic_reconfigure/server.h>
#include <Eigen/Dense>
#include <fstream>
#include <geometry_msgs/Point.h>
#include <image_transport/image_transport.h>
#include <iostream>
#include <math.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <nodelet/nodelet.h>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/common/intersections.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/impl/extract_indices.hpp>
#include <pcl/filters/impl/passthrough.hpp>
#include <pcl/filters/impl/project_inliers.hpp>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_cloud.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/impl/sac_segmentation.hpp>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pluginlib/class_list_macros.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Int8.h>
#include <string>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <utility>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

using namespace cv;
typedef message_filters::Subscriber<sensor_msgs::Image> image_sub_type;
typedef message_filters::Subscriber<sensor_msgs::PointCloud2> pc_sub_type;

namespace extrinsic_calibration
{
class feature_extraction : public nodelet::Nodelet
{
public:
  feature_extraction();
  void extractROI(const sensor_msgs::Image::ConstPtr& img, const sensor_msgs::PointCloud2::ConstPtr& pc);
  void flagCallback(const std_msgs::Int8::ConstPtr& msg);
  void boundsCallback(cam_lidar_calibration::boundsConfig& config, uint32_t level);
  double* convertToImgPts(double x, double y, double z);

  void cloudPassthrough(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_in,
                        pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_passthrough, const std::string field_name,
                        const float limit_min, const float limit_max);

  void cloudPassthrough(pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_passthrough, const std::string field_name,
                        const float limit_min, const float limit_max);

  void projectInliersPlane(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_in,
                           pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_projected,
                           pcl::ModelCoefficients::Ptr& coefficients);

  bool extractPlaneSegmentation(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& points_in,
                                pcl::ModelCoefficients::Ptr& coefficients, pcl::PointIndices::Ptr& inliers);

  void findEdgePoints(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& points_in,
                      pcl::PointCloud<pcl::PointXYZIR>::Ptr& right_edge_points,
                      pcl::PointCloud<pcl::PointXYZIR>::Ptr& left_edge_points);

  bool extractLinesSegmentation(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& edge_points,
                                pcl::ModelCoefficients::Ptr& coefficients_1, pcl::PointIndices::Ptr& inliers_1,
                                pcl::ModelCoefficients::Ptr& coefficients_2, pcl::PointIndices::Ptr& inliers_2);

  bool extractLinesHybrid(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& left_edge_points,
                          pcl::ModelCoefficients::Ptr& coefficients_left_1, pcl::PointIndices::Ptr& inliers_left_1,
                          pcl::ModelCoefficients::Ptr& coefficients_left_2, pcl::PointIndices::Ptr& inliers_left_2,
                          const bool is_left);
  void add_example_set();

private:
  struct InitialParameters
  {
    std::string camera_topic;
    std::string lidar_topic;
    bool fisheye_model;
    int lidar_ring_count;
    std::pair<int, int> grid_size;
    int square_length;                         // in millimetres
    std::pair<int, int> board_dimension;       // in millimetres
    std::pair<int, int> cb_translation_error;  // in millimetres
    cv::Mat camera_mat;
    int dist_coeff_num;
    cv::Mat dist_coeff;
  } i_params;

  std::string pkg_loc;
  int cb_l, cb_b, l, b, e_l, e_b;

  virtual void onInit();

  ros::Publisher pub;
  cv::FileStorage fs;
  int flag = 0;
  cam_lidar_calibration::boundsConfig bound;
  cam_lidar_calibration::calibration_data sample_data;
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::PointCloud2> MySyncPolicy;
  message_filters::Synchronizer<MySyncPolicy>* sync;
  int queue_rate = 5;

  ros::Publisher roi_publisher;
  ros::Publisher pub_cloud;
  ros::Publisher pub_exp_region;
  ros::Publisher vis_pub;
  ros::Publisher cb_pub;
  image_transport::Publisher image_publisher;
  ros::Subscriber flag_subscriber;
  message_filters::Subscriber<sensor_msgs::Image>* image_sub;
  message_filters::Subscriber<sensor_msgs::PointCloud2>* pcl_sub;
  visualization_msgs::Marker board_norm_vec_marker;
  boost::shared_ptr<image_transport::ImageTransport> it_;
  boost::shared_ptr<image_transport::ImageTransport> it_p_;
  boost::shared_ptr<dynamic_reconfigure::Server<cam_lidar_calibration::boundsConfig>> server;
};

PLUGINLIB_EXPORT_CLASS(extrinsic_calibration::feature_extraction, nodelet::Nodelet);
}  // namespace extrinsic_calibration
