#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <termios.h>

int getch()
{
  static struct termios oldt, newt;
  tcgetattr(STDIN_FILENO, &oldt);  // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                // disable buffering
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);  // apply new settings
  int c = getchar();                        // read character (non-blocking)
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

void show_usage()
{
  std::cout << " ----- Press an appropriate key ----- " << std::endl;
  std::cout << " 'i' to take an input sample" << std::endl;
  std::cout << " 'd' to delete the last sample" << std::endl;
  std::cout << " 'o' to start the optimization process" << std::endl;
  std::cout << " 's' to add example samples" << std::endl;
  std::cout << " 'e' to end the calibration process" << std::endl;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "input_sample");
  ros::NodeHandle sample("~");
  ros::Publisher sample_publisher;
  std_msgs::Int8 flag;
  sample_publisher = sample.advertise<std_msgs::Int8>("/flag", 20);

  while (sample_publisher.getNumSubscribers() == 0)
  {
    ROS_WARN("Waiting for a subscriber ...");
    sleep(2);
  }

  ROS_INFO("Found a subscriber!");
  show_usage();
  ros::spinOnce();

  while (ros::ok())
  {
    int c = getch();
    if (c == 'i')  // flag to take an input sample
    {
      flag.data = 1;
      sample_publisher.publish(flag);
    }
    else if (c == 'o')  // flag to start optimization
    {
      flag.data = 2;
      sample_publisher.publish(flag);
    }
    else if (c == 'd')  // flag to delete the last sample
    {
      flag.data = 3;
      sample_publisher.publish(flag);
    }
    else if (c == '\n')  // flag to use the input sample
    {
      flag.data = 4;
      sample_publisher.publish(flag);
    }
    else if (c == 's')  // flag to load example samples
    {
      flag.data = 5;
      sample_publisher.publish(flag);
    }
    else if (c == 'e')  // flag to terminate the node; if you do Ctrl+C, press enter after that
    {
      ros::shutdown();
    }
    else
    {
      ROS_WARN("%c - not a valid key", c);
      show_usage();
    }
  }
  return 0;
}
